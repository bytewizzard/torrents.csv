require('dotenv').config();
const fs = require('fs');

exports.setEnv =  function() {
  if (process.env.TORRENTS_CSV_ENDPOINT) {
    let env_line = `export const endpoint = '${process.env.TORRENTS_CSV_ENDPOINT}';`;
    fs.writeFile("src/env.ts", env_line, function (err) {
      if (err) {
        return console.log(err);
      }
      console.log("Found $TORRENTS_CSV_ENDPOINT, wrote src/env.ts");
      console.log("\t" + env_line);
    });
  } else {
    console.log("No $TORRENTS_CSV_ENDPOINT environment variable set, using http://0.0.0.0:8080");
  }
}
